# PHPlattenbau

A collection of read-to-use PHP blocks for building PHP applications with a minimum of effort. A block is essentially a simple application that performs a task, employing one or several useful techniques.

## List of blocks

- **template** Basic PHP page template.
- **password-protect** Simple password protection solution using cookies.
- **fetch-markdown** Fetch the contents of a remote file via URL. Save the contents as a Markdown file. Read the saved file and convert it into HTML.
- **edit-text** Create file. Read the file and edit its contents using an HTML text area. Save the file and a backup file with a date stamp.
- **shell-exec** Get a list of all files in a directory. Create a selection list. Use the POST method to readto the selected item. Use `shell_exec` to run a system command. Requires the _mpg123_ player.
- **csv-to-table** Read CSV file and render its contents as an HTML table.
- **get-json** Use the GET method to read values from a URL. Use an API call to fetch data in the JSON format. Process and display JSON data. Use the _http://127.0.0.1:8000/?city=Tokyo&country=JP_ as an example URL.
- **parse-xml** Read text file line by line and populate a selection list. Fetch an RSS feed and process XML data.
- **weight-tracker** A complete demo application built with PHPlattenbau blocks.

## What can you build with PHPlattenbau?

- [Linkalot](https://gitlab.com/dmpop/linkalot) bookmark manager
- [micro.sth](https://gitlab.com/dmpop/microsth) micro-blogging application
- [Weather and notes](https://gitlab.com/dmpop/weather-and-notes) weather and note tracking tool

## Author

[Dmitri Popov](https://tokyomade.photography/)

# License

The [GNU General Public License version 3](http://www.gnu.org/licenses/gpl-3.0.en.html)
