<!DOCTYPE html>
<html lang="en">
    <!-- Author: Dmitri Popov, dmpop@linux.com
	 License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt --> 
    <head>
	<meta charset="utf-8">
	<title>RSS reader</title>
	<link rel="icon"
		href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>🏗️</text></svg>">
	<link rel="stylesheet" href="lit.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
	<div class="c">
	    <h1>RSS reader</h1>
	    <hr>
	    <form action=" " method="POST">
		<p>Select RSS feed: </p>
		<select class="card w-100" name="rss">
		    <?php
		    $feed_list = fopen("feeds.txt","r");
		    while(! feof($feed_list)) {
			$feed = str_replace(PHP_EOL, '', fgets($feed_list));
			echo "<option value='$feed'>$feed</option>";
		    }
		    fclose($feed_list);
		    ?>
		</select>
		<p></p>
		<input class="btn primary" type="submit" value="Show feed" name="show">
	    </form>
	    <?php
	    if (isset($_POST['show'])) {
		$rss = simplexml_load_file($_POST['rss']);
		echo '<h3>'. $rss->channel->title . '</h3>';
		foreach ($rss->channel->item as $item) {
		    echo '<h3><a href="'. $item->link .'">' . $item->title . "</a></h3>";
		    echo "<p>" . $item->description . "</p>";
		}
	    } 
	    ?>
	    <hr>
	    <p>
		<a href='https://gitlab.com/dmpop/phplattenbau'>PHPlattenbau</a>
	    </p>
	</div>
    </body>
</html>
