<?php
include('config.php');
if (empty($_COOKIE['password']) || $_COOKIE['password'] !== $passwd) {
    header('Location: login.php');
    exit;
}
?>
