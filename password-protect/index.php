<?php
include('config.php');
if ($protect) {
    require_once('protect.php');
}
?>
<html lang="en">
    <!-- Author: Dmitri Popov, dmpop@linux.com
	 License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt -->
    <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Password protection</title>
	<link rel="icon"
		href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>🏗️</text></svg>">
	<link rel="stylesheet" href="lit.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
	<div class="c">
	    <h1>Password protection</h1>
	    <hr>
	    <p>
		Success!
	    </p>
	</div>
    </body>
</html>
