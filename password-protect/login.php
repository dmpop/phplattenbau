<?php
include('config.php');

/* Redirect here after login */
$redirect_after_login = 'index.php';

/* Set timezone to UTC */
date_default_timezone_set('UTC');

/* Don't ask for password again for */
$remember_password = strtotime('+30 days'); // 30 days

if (isset($_POST['password']) && $_POST['password'] == $passwd) {
    setcookie("password", $passwd, $remember_password);
    header('Location: ' . $redirect_after_login);
    exit;
}
?>
<html lang="en">
    <!-- Author: Dmitri Popov, dmpop@linux.com
	 License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt -->
    <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Login</title>
	<link rel="stylesheet" href="lit.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
	<div class="c">
	    <h1>Log in</h1>
	    <hr>
		<p>Type password and press <b>Enter</b> to log in</p>
        <form method="POST">
	    Password: <input type="password" name="password">
        </form>
	</div>
    </body>
</html>
