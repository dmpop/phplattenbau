<html lang="en">
    <!-- Author: Dmitri Popov, dmpop@linux.com
         License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt -->
    <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>CSV to HTML table</title>
	<link rel="icon"
		href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>🏗️</text></svg>">
	<link rel="stylesheet" href="lit.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
	<div class="c">
	    <h1>CSV to HTML table</h1>
	    <hr>
	    <table class="w-100">
		<?php
		$csv_file = "data.csv"; 
		$delimiter =";";
		if(!is_file($csv_file))
		{
		    $init_data = "Column 1; Column 2; Column 3\nField 1; Field 2; Field 3\n";
		    file_put_contents($csv_file, $init_data);
		}
		$row = 1;
		if (($handle = fopen($csv_file, "r")) !== FALSE) {
		    while (($data = fgetcsv($handle, 0, $delimiter)) !== FALSE) {
			$num = count($data);
			if ($row == 1) {
			    echo '<thead><tr>';
			}else{
			    echo '<tr>';
			}
			for ($c=0; $c < $num; $c++) {
			    if(empty($data[$c])) {
				$value = "&nbsp;";
			    }else{
				$value = $data[$c];
			    }
			    if ($row == 1) {
				echo '<th>'.$value.'</th>';
			    }else{
				echo '<td>'.$value.'</td>';
			    }
			}
			if ($row == 1) {
			    echo '</tr></thead><tbody>';
			}else{
			    echo '</tr>';
			}
			$row++;
		    }
		    fclose($handle);
		}
		?>
            </tbody>
	    </table>
	    <hr>
	    <p>
		<a href='https://gitlab.com/dmpop/phplattenbau'>PHPlattenbau</a>
	    </p>
	</div>
    </body>
</html>
