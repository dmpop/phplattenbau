<html lang='en'>
    <!-- Author: Dmitri Popov, dmpop@linux.com
         License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt -->
    <head>
	<meta charset="utf-8">
	<title>Edit text</title>
	<link rel="icon"
		href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>🏗️</text></svg>">
	<link rel="stylesheet" href="lit.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
	<div class="c">
	    <h1>Edit text</h1>
	    <hr>
            <?php
	    $dir = "files/";
	    if (!file_exists($dir)) {
		mkdir($dir, 0777, true);
	    }
	    $txt_file = $dir."text.txt";
            ?>
            <?php
            if (isset($_POST["submit"])){
		copy($txt_file, $dir.date('Y-m-d-H-i-s').'.txt');
		$fp = fopen($txt_file, "w");
		$data = $_POST["text"];
		fwrite($fp, $data);
		fclose($fp);
            };
            ?>
	    <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
		<textarea class="card w-100" name="text"><?php echo file_get_contents($txt_file); ?></textarea><br>
		<input class="btn primary" type="submit" name="submit" value="Save">
	    </form>
	    <hr>
	    <p>
		<a href='https://gitlab.com/dmpop/phplattenbau'>PHPlattenbau</a>
	    </p>
	</div>
    </body>
</html>
