<?php
include('config.php');
include('inc/parsedown.php');
// Error reporting
error_reporting(E_ERROR);
?>

<html lang="en">
    
    <!-- Author: Dmitri Popov, dmpop@linux.com
         License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt -->
    
    <head>
	<meta charset="utf-8">
	<title><?php echo $title ?></title>
	<link rel="icon"
		href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>🏗️</text></svg>">
	<link rel="stylesheet" href="css/lit.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
	<div class="c">
	    <h1><?php echo $title ?></h1>
	    <hr>
	    <p>This is a PHPlatenbau template.</p>
	    <?php
	    $Parsedown = new Parsedown();
            echo $Parsedown->text("```Insert your PHP code here.```");
	    ?>
	    <hr>
	    <p><?php echo $footer; ?></p>
	</div>
    </body>
</html>
