<!DOCTYPE html>
<html lang="en">
    <!-- Author: Dmitri Popov, dmpop@linux.com
	 License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt --> 
    <head>
	<meta charset="utf-8">
	<title>Play MP3</title>
	<link rel="icon"
		href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>🏗️</text></svg>">
	<link rel="stylesheet" href="lit.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
	<div class="c">
	    <h1>Play MP3</h1>
	    <hr>
	    <form action=" " method="POST">
		<p>Select track: </p>
		<select class="card w-100" name="mp3">
		    <?php
		    $dir = "mp3";
		    if (!file_exists($dir)) {
			mkdir($dir, 0777, true);
		    }
		    $files = glob($dir."/*");
		    foreach ($files as $file) {
			$filename = basename($file);
			$name = pathinfo($file)['extension'];
			echo "<option value='$filename'>".pathinfo($file)['filename']."</option>"; 
		    }
		    ?>
		</select>
		<p></p>
		<input class="btn primary" style='display: inline!important;' type="submit" value="Play" name="play">
		<input class="btn" style='display: inline!important;' type="submit" value="Stop" name="stop">
	    </form>
	    <?php
	    if (isset($_POST['play']))
	    {
		shell_exec('mpg123 mp3/'.escapeshellarg($_POST['mp3']).' > /dev/null 2>&1 & echo $!');
		echo "Now playing: ".($_POST['mp3']);
	    }
	    if (isset($_POST['stop']))
	    {
		shell_exec('killall mpg123 > /dev/null 2>&1 & echo $!');
	    }
	    ?>
	    <p>
		<details>
		    <summary>Help</summary>
		    <p>Usage:</p>
		    <ul>
			<li>
			    Make sure that the the <i>mp3</i> directory contains MP3 files.
			</li>
			<li>
			    Choose the desired track, press <b>Play</b>
			</li>
			<li>
			    Press <b>Stop</b> to stop playing
			</li>
		    </ul>
		</details>
	    </p>
	    <hr>
	    <p>
		<a href='https://gitlab.com/dmpop/phplattenbau'>PHPlattenbau</a>
	    </p>
	</div>
    </body>
</html>
