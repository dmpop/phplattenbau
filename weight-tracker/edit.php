<html lang='en'>
    <!-- Author: Dmitri Popov, dmpop@linux.com
         License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt -->
    <head>
	<meta charset="utf-8">
	<title>Edit data</title>
	<link rel="icon"
		href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>🏗️</text></svg>">
	<link rel="stylesheet" href="lit.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
	<div class="c">
	    <h1>Edit data</h1>
	    <hr>
            <?php
	    $csv_file = "data.csv";
            if (isset($_POST["submit"])){
		$file = fopen($csv_file, "w");
		$content = $_POST["text"];
		fwrite($file, $content);
		fclose($file);
            };
            ?>
	    <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
		<textarea class="card w-100" style="height: 25em;" name="text"><?php echo file_get_contents($csv_file); ?></textarea><br>
		<input class="btn primary" type="submit" name="submit" value="Save">
		<a class="btn" href="index.php">Back</a>
	    </form>
	    <hr>
	    <p>
		Built with <a href='https://gitlab.com/dmpop/phplattenbau'>PHPlattenbau</a>
	    </p>
	</div>
    </body>
</html>
