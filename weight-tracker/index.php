<?php
include('config.php');
if ($protect) {
    require_once('protect.php');
}
?>
<html lang="en">
    <!-- Author: Dmitri Popov, dmpop@linux.com
         License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt -->
    <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Weight tracker</title>
	<link rel="icon"
		href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>🏗️</text></svg>">
	<link rel="stylesheet" href="lit.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
	<div class="c">
	    <h1>Weight tracker</h1>
	    <hr>
	    <table class="w-100">
		<?php
		$csv_file = "data.csv";
		$delimiter =",";
		$date = date('Y-m-d');
		?>
		<form method='POST' action=''>
		    <label for='weight'>Weight:</label>&nbsp;
		    <input type='text' name='weight'>&nbsp;
		    <input class="btn primary"  type="submit" name="save" value="Save">&nbsp;
		    <a class="btn" href="edit.php">Edit</a>
		</form>
		<?php
		if(!is_file($csv_file))
		{
		    $init_data = "Date, Weight\n";
		    file_put_contents($csv_file, $init_data);
		}
		if(isset($_POST['save'])) {
		    $file = fopen($csv_file, "a");
		    $weight = $_POST["weight"];
		    fwrite($file, $date.$delimiter." ".$weight."\n");
		    fclose($file);
		}
		$row = 1;
		if (($handle = fopen($csv_file, "r")) !== FALSE) {
		    while (($data = fgetcsv($handle, 0, $delimiter)) !== FALSE) {
			$num = count($data);
			if ($row == 1) {
			    echo '<thead><tr>';
			}else{
			    echo '<tr>';
			}
			for ($c=0; $c < $num; $c++) {
			    if(empty($data[$c])) {
				$value = "&nbsp;";
			    }else{
				$value = $data[$c];
			    }
			    if ($row == 1) {
				echo '<th>'.$value.'</th>';
			    }else{
				echo '<td>'.$value.'</td>';
			    }
			}
			if ($row == 1) {
			    echo '</tr></thead><tbody>';
			}else{
			    echo '</tr>';
			}
			$row++;
		    }
		    fclose($handle);
		}
		?>
            </tbody>
	    </table>
	    <hr>
	    <p>
		Built with <a href='https://gitlab.com/dmpop/phplattenbau'>PHPlattenbau</a>
	    </p>
	</div>
    </body>
</html>
