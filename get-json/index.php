<?php
include('config.php');
if (empty($api_key)) {
    exit("API key is not provided.");
}
?>
<html lang="en">
    <!-- Author: Dmitri Popov, dmpop@linux.com
	 License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt -->
    <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Get weather info</title>
	<link rel="icon"
		href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>🏗️</text></svg>">
	<link rel="stylesheet" href="lit.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
	<div class="c">
	    <h1>Get weather info</h1>
	    <hr>
	    <?php
	    // http://127.0.0.1:8000/??city=Tokyo&country=JP
	    $city = $_GET["city"];
	    $country = $_GET["country"];
	    $request = "http://api.openweathermap.org/data/2.5/weather?APPID=$api_key&q=$city,$country&units=metric&cnt=7&lang=en&units=metric&cnt=7&lang=en";
	    $response = file_get_contents($request);
	    $data = json_decode($response,true);
	    echo "<h2>Weather in ".$city."</h2>";
	    $temp = $data['main']['temp_max'];
	    $weather = $data['weather'][0]['main'];
	    $wind = $data['wind']['speed'];
	    echo "Temperature: ".$temp."°C<br>";
	    echo "Current conditions: ".$weather."<br>";
	    echo "Wind speed: ".$wind."m/s<br>";
	    ?>
	    <hr>
	    <p>
		<a href='https://gitlab.com/dmpop/phplattenbau'>PHPlattenbau</a>
	    </p>
	</div>
    </body>
</html>
